#include <Arduino.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>

#include "mqtt_client.h"
#include "device_status.h"
#include "settings.h"

// const char *server = "iot.eclipse.org";
WiFiClient wifiClient;

void callback(char *topic, byte *payload, unsigned int length)
{
    // handle message arrived
}

PubSubClient psClient;
bool psClientInited = false;

/**
 * Using the server ip and port stored in settings, the PubSubClient will be
 * initialized
 */
void mqtt_set_up_client(void)
{
    psClient = PubSubClient(*settings_get_mqtt_server_ip(),
                            settings_get_mqtt_server_port(),
                            callback,
                            wifiClient);

    psClientInited = true;
}

/**
 * Initialize the MQTT connection
 * 
 * If the the PubSubClient client is not initialized, it will be prior to
 * connecting
 *
 * @return True, if connection was successful
 */
bool mqtt_connect(void)
{
    if (!psClientInited) {
        mqtt_set_up_client();
    }

    if (! psClient.connected()) {

        Serial.print("Connecting to ");
        Serial.print(*settings_get_mqtt_server_ip());
        Serial.print(":");
        Serial.print(settings_get_mqtt_server_port());
        Serial.print(" as ");
        Serial.println(settings_get_device_name());

        if (psClient.connect(settings_get_device_name())) {
            Serial.println("Connected to MQTT broker");
        } else {
            Serial.println("MQTT connect failed");
        }
    }

    return psClient.connected();
}

/**
 * Publishes the given text to the given topic
 */
void mqtt_publish_str_to_topic(const char *toPub, const char *inTopic)
{
    Serial.printf("Publish (%s): %s\r\n", inTopic, toPub);

    if (psClient.connected()) {
        if (psClient.publish(inTopic, toPub) ) {
            Serial.println("Publish ok");

            /* Wait some time for the message to get sent */
            delay(50);
        } else {
            Serial.println("Publish failed");
        }
    }
}

/**
 * Disconnect from the MQTT broker
 */
void mqtt_disconnect(void)
{
    Serial.println("Disconnecting from MQTT Broker");
    psClient.disconnect();

    /* Wait for the device to disconnect */
    while(psClient.loop());

    Serial.printf("State: %i\r\n", psClient.state());
}

/**
 * To be called regularly in order to maintain connection with hte server
 */
void mqtt_loop(void)
{
    psClient.loop();
}