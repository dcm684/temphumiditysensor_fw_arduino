#include <stdint.h>
#include <pgmspace.h>


#include "settings.h"
#include "enviro.h"
#include "mqtt_client.h"
#include "device_status.h"

typedef enum {
    TOPIC_TYPE_TEMPERATURE,
    TOPIC_TYPE_HUMIDITY,
    TOPIC_TYPE_VOLTAGE,
    TOPIC_TYPE_COUNT
} topic_type;

const char DATA_SUFFIX[] PROGMEM = "state";
const char CONFIG_SUFFIX[] PROGMEM = "config";
const char DATA_FORMAT_STRING[] PROGMEM = "{\"temperature\":%.1f, \"humidity\":%.1f, \"battery\":%u}";
const char CONFIG_TEMPERATURE_STRING[] PROGMEM =
    "{\"device_class\": \"temperature\", "
    "\"name\": \"Temperature\", "
    "\"state_topic\": \"%s\", "
    "\"unit_of_measurement\": \"°C\", "
    "\"value_template\": \"{{ value_json.temperature}}\"}";
const char CONFIG_HUMIDITY_STRING[] PROGMEM =
    "{\"device_class\": \"humidity\", "
    "\"name\": \"Humidity\", "
    "\"state_topic\": \"%s\", "
    "\"unit_of_measurement\": \"%%\", "
    "\"value_template\": \"{{ value_json.humidity}}\"}";
const char CONFIG_VOLTAGE_STRING[] PROGMEM =
    "{\"device_class\": \"battery\", "
    "\"name\": \"Battery\", "
    "\"state_topic\": \"%s\", "
    "\"unit_of_measurement\": \"%%\", "
    "\"value_template\": \"{{ value_json.battery}}\"}";

/* Topic for JSON data to be published to. No +1 for null char since
DATA_SUFFIX calc already includes it */
char dataTopic[MAX_MQTT_PREFIX_LEN + 1 +
                                   MAX_DEVICE_NAME_LEN + 1 +
                                   sizeof(DATA_SUFFIX)/sizeof(DATA_SUFFIX[0])];

char baseConfigTopic[MAX_MQTT_PREFIX_LEN + 1 +
                                         MAX_DEVICE_NAME_LEN + 1 + 2 +
                                         sizeof(CONFIG_SUFFIX)/sizeof(CONFIG_SUFFIX[0])];

/**
 * Initialize the dataTopic topic and baseConfigTopic topic strings
 *
 * Data message topic: prefix/deviceName/DATA_SUFFIX
 *
 * The baseConfigTopic topic will need to be run through printf to indicate
 * the sensor specfic config message, e.g sensorH, sensorT, sensorV... Upon
 * exiting this function it will be sensor%c
 */
void mqtt_init_topics(void)
{
    strlcpy(dataTopic, settings_get_mqtt_topic_prefix(), MAX_MQTT_PREFIX_LEN);
    strlcat(dataTopic, "/", sizeof(dataTopic));

    strlcat(dataTopic, settings_get_device_name(), MAX_DEVICE_NAME_LEN);
    /* Up to the end of the device, the data and config topics are identical */
    strlcpy(baseConfigTopic, dataTopic, sizeof(baseConfigTopic));
    strlcat(dataTopic, "/", sizeof(dataTopic));

    strcat_P(dataTopic, DATA_SUFFIX);

    /* Now finish of the config format string */
    /* Add a c format string for the sensor type. To be populated later */
    strlcat(baseConfigTopic, "%c", sizeof(baseConfigTopic));
    strlcat(baseConfigTopic, "/", sizeof(baseConfigTopic));

    strcat_P(baseConfigTopic, CONFIG_SUFFIX);
}

/**
 * Sends the json data payload to topic
 */
void mqtt_send_json_data(float tempInC, float humidity, int batteryLevel) {
    char outString[strlen_P(DATA_FORMAT_STRING) + (-4 + 5) + (-4 + 5) + (-2 + 2)];

    /* Convert the ADC reading to percentage of range */
    uint32_t relativeVoltage;
    relativeVoltage = (unsigned int) batteryLevel;
    relativeVoltage = (relativeVoltage * 100) / 1024;

    snprintf_P(outString, sizeof(outString), DATA_FORMAT_STRING,
               tempInC,
               humidity,
               relativeVoltage);

    mqtt_publish_str_to_topic(outString, dataTopic);
}

/**
 * Sends all current values
 *
 * Sends
 *      * Temperature (C)
 *      * Humidity (%)
 *      * Voltage (%)
 */
void mqtt_send_all_data(void) {
    mqtt_send_json_data(enviro_getTemperatureCelsius(),
                        enviro_getHumidity(),
                        device_get_supply_voltage());
}

/**
 * Sends the HomeAssistant configuration message for temperature
 */
void mqtt_send_HA_temperature_config(void)
{
    char outString[strlen_P(CONFIG_TEMPERATURE_STRING) + strlen(dataTopic) + 1];

    snprintf_P(outString, sizeof(outString), CONFIG_TEMPERATURE_STRING, dataTopic);

    char outTopic[sizeof(baseConfigTopic)];

    snprintf(outTopic, sizeof(outTopic), baseConfigTopic, 'T');

    mqtt_publish_str_to_topic(outString, outTopic);
}

/**
 * Sends the HomeAssistant configuration message for the given topic type
 * 
 * @param inTopic Topic to send config message for
 */
void mqtt_send_HA_config(topic_type inTopic)
{
    char typeChar;
    /* Temperature is longest name. I know terrible coding. Uggh. */
    char outString[strlen_P(CONFIG_TEMPERATURE_STRING) + strlen(dataTopic) + 1];

    switch (inTopic) {
    case TOPIC_TYPE_HUMIDITY:
        typeChar = 'H';
        snprintf_P(outString, sizeof(outString), CONFIG_HUMIDITY_STRING, dataTopic);
        break;
    case TOPIC_TYPE_TEMPERATURE:
        snprintf_P(outString, sizeof(outString), CONFIG_TEMPERATURE_STRING, dataTopic);
        typeChar = 'T';
        break;
    case TOPIC_TYPE_VOLTAGE:
        snprintf_P(outString, sizeof(outString), CONFIG_VOLTAGE_STRING, dataTopic);
        typeChar = 'V';
        break;
    default:
        return;
    }

    char outTopic[sizeof(baseConfigTopic)];

    snprintf(outTopic, sizeof(outTopic), baseConfigTopic, typeChar);

    mqtt_publish_str_to_topic(outString, outTopic);
}

/**
 * Send all HomeAssistant configuration messages
 */
void mqtt_send_all_HA_config(void)
{
    mqtt_send_HA_config(TOPIC_TYPE_TEMPERATURE);
    mqtt_send_HA_config(TOPIC_TYPE_HUMIDITY);
    mqtt_send_HA_config(TOPIC_TYPE_VOLTAGE);
}