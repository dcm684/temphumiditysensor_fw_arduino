#ifndef MQTT_TOPIC_H
#define MQTT_TOPIC_H
#include <stdint.h>

#ifdef __cplusplus
extern "C" {}
#endif

void mqtt_init_topics(void);
void mqtt_send_all_data(void);
void mqtt_send_all_HA_config(void);

#endif /* MQTT_TOPIC_H */