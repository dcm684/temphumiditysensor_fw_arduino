#include <Arduino.h>
#include <EEPROM.h>
// #include <EEPROM_rotate.h>
#include <ESP8266WiFi.h>

#include "serial.h"
#include "settings.h"
#include "math_crc.h"

// extern EEPROM_Rotate persistentStorage;
extern EEPROMClass persistentStorage;

enum {
    CHAR_CMD_DUMP_ALL = 'd',
    CHAR_CMD_SET_ALL = 's'
};

enum {
    CHAR_TYPE_SERVER_IP = 0xF0,
    CHAR_TYPE_SERVER_PORT = 0xF1,
    CHAR_TYPE_CLIENT_IP = 0xF2,
    CHAR_TYPE_SUBNET_IP = 0xF3,
    CHAR_TYPE_GATEWAY_IP = 0xF4,
    CHAR_TYPE_WIFI_SSID = 0xF5,
    CHAR_TYPE_WIFI_PASSPHASE = 0xF6,
    CHAR_TYPE_DEVICE_NAME = 0xF7,
    CHAR_TYPE_MQTT_PREFIX = 0xF8,
    CHAR_TYPE_SLEEP_TIME = 0xF9,
    CHAR_TYPE_EOT = 0xE0,
    CHAR_TYPE_ERROR = 0xEF
};

enum {
    CHAR_MESSAGE_SET_SUCCESS = 0xDE,
    CHAR_MESSAGE_SET_FAILURE = 0xAD
};

void serial_dump_all_settings(void);
void serial_set_settings(void);

void serial_dump_ip(IPAddress *ipToDump);
bool serial_load_ip(IPAddress *ipRead);
void serial_dump_string(String *inString);
bool serial_load_string(String *inString);

void serial_load_mqtt_server_ip(void);
void serial_dump_mqtt_server_ip(void);

void serial_load_mqtt_server_port(void);
void serial_dump_mqtt_server_port(void);

void serial_load_mqtt_client_ip(void);
void serial_dump_mqtt_client_ip(void);

void serial_load_wlan_gateway(void);
void serial_dump_wlan_gateway(void);

void serial_load_wlan_subnet(void);
void serial_dump_wlan_subnet(void);

void serial_load_wifi_ssid(void);
void serial_dump_wifi_ssid(void);

void serial_load_wifi_key(void);
void serial_dump_wifi_key(void);

void serial_load_device_name(void);
void serial_dump_device_name(void);

void serial_dump_mqtt_topic_prefix(void);
void serial_load_mqtt_topic_prefix(void);

void serial_dump_sleep_time(void);
void serial_load_sleep_time(void);

void init_serial(void)
{
    Serial.begin(115200);
    Serial.setTimeout(2000);

    // Wait for serial to initialize.
    while (!Serial);
}

/**
 * Looks and processes for incoming serial commands
 */
void check_and_process_serial(void)
{
    if (Serial.available() > 0) {
        char readByte;
        Serial.readBytes(&readByte, 1);

        switch (readByte) {

        case CHAR_CMD_DUMP_ALL:
            serial_dump_all_settings();
            break;

        case CHAR_CMD_SET_ALL:
            serial_set_settings();
            break;

        default:
            break;
        }
    }
}

/**
 * Dumps all of the settings out over serial
 */
void serial_dump_all_settings(void)
{
    Serial.write(CHAR_CMD_DUMP_ALL);
    serial_dump_mqtt_server_ip();
    serial_dump_mqtt_server_port();
    serial_dump_mqtt_client_ip();
    serial_dump_wlan_gateway();
    serial_dump_wlan_subnet();
    serial_dump_wifi_ssid();
    serial_dump_wifi_key();
    serial_dump_device_name();
    serial_dump_mqtt_topic_prefix();
    serial_dump_sleep_time();

    Serial.write(CHAR_TYPE_EOT);
}

/**
 * Allow setting of the settings via the serial connection
 *
 * For a setting to be set, that settings type byte must be received followed
 * by the requisite data and checksum.
 *
 * If an invalid message type is received CHAR_TYPE_ERROR will be sent back.
 * This function provides no validation. Each setting handler is expected to
 * handle validation.
 *
 * Function will exit after receiving CHAR_TYPE_EOT
 *
 * Will commit data to storage upon exiting. If no changes were made, there is
 * no harm since the EEPROM is still "clean"
 */
void serial_set_settings(void)
{
    unsigned long timeoutTime;
    bool exitFunction = false;

    Serial.write(CHAR_CMD_SET_ALL);

    while(1) {
        /* Hold until some data is received or the system times out */
        timeoutTime = millis() + 100;
        if (!exitFunction) {
            while((Serial.available() < 1) && (millis() < timeoutTime));
        }

        /* Time out */
        if (exitFunction || (Serial.available() < 1)) {
            persistentStorage.commit();
            return;
        }

        /* A command was received, get the command byte */
        char readChar;

        Serial.readBytes(&readChar, 1);

        switch(readChar) {
        case CHAR_TYPE_SERVER_IP:
            serial_load_mqtt_server_ip();
            break;

        case CHAR_TYPE_CLIENT_IP:
            serial_load_mqtt_client_ip();
            break;

        case CHAR_TYPE_GATEWAY_IP:
            serial_load_wlan_gateway();
            break;

        case CHAR_TYPE_SUBNET_IP:
            serial_load_wlan_subnet();
            break;

        case CHAR_TYPE_SERVER_PORT:
            serial_load_mqtt_server_port();
            break;

        case CHAR_TYPE_WIFI_SSID:
            serial_load_wifi_ssid();
            break;

        case CHAR_TYPE_WIFI_PASSPHASE:
            serial_load_wifi_key();
            break;

        case CHAR_TYPE_DEVICE_NAME:
            serial_load_device_name();
            break;

        case CHAR_TYPE_MQTT_PREFIX:
            serial_load_mqtt_topic_prefix();
            break;

        case CHAR_TYPE_SLEEP_TIME:
            serial_load_sleep_time();
            break;

        case CHAR_TYPE_EOT:
            Serial.write(CHAR_TYPE_EOT);
            exitFunction = true;
            break;

        default:
            Serial.write(CHAR_TYPE_ERROR);
            Serial.write(readChar);
            break;
        }
    }
}

/**
 * Sends the MQTT server's IP address out over serial with an identifier and
 * its CRC
 */
void serial_dump_mqtt_server_ip(void)
{
    IPAddress *outIP;

    /* Send the byte indicating the type of message */
    Serial.write(CHAR_TYPE_SERVER_IP);

    outIP = settings_get_mqtt_server_ip();
    serial_dump_ip(outIP);
}

/**
 * Reads the MQTT server's IP address received via the serial port
 *
 * Order of message
 * Identifier
 * Server IP
 * CRC of IP
 *
 * It is expected that the identifier has already been read
 *
 * This function will respond
 * CHAR_TYPE_SERVER_IP
 * Result possibilities:
 *  CHAR_MESSAGE_SET_SUCCESS
 *  CHAR_MESSAGE_SET_FAILURE
 */
void serial_load_mqtt_server_ip(void)
{
    IPAddress inIP;

    Serial.write(CHAR_TYPE_SERVER_IP);

    if (serial_load_ip(&inIP)) {
        settings_set_mqtt_server_ip(&inIP);

        Serial.write(CHAR_MESSAGE_SET_SUCCESS);

    } else {
        Serial.write(CHAR_MESSAGE_SET_FAILURE);
    }
}

/**
 * Sends the MQTT server's port out over serial with an identifier and
 * its CRC
 *
 * Little end - LSb of port in first byte, MSb in second
 */
void serial_dump_mqtt_server_port(void)
{
    uint16_t port = settings_get_mqtt_server_port();

    Serial.write(CHAR_TYPE_SERVER_PORT);

    Serial.write(port & 0xFF);
    Serial.write(port >> 8);

    Serial.write(CRC8((uint8_t *) &port, 2));
}

/**
 * Read's the MQTT server's port via serial
 *
 * Order of message
 * Identifier
 * Port (LSb port in first byte, MSb in second)
 * CRC of port
 *
 * It is expected that the identifier has already been read
 *
 * This function will respond
 * CHAR_TYPE_SERVER_PORT
 * Result possibilities:
 *  CHAR_MESSAGE_SET_SUCCESS
 *  CHAR_MESSAGE_SET_FAILURE
 */
void serial_load_mqtt_server_port(void)
{
    bool validRead = false;
    uint16_t port;

    Serial.write(CHAR_TYPE_SERVER_PORT);

    if (Serial.readBytes((uint8_t *) &port, 2) == 2) {
        uint8_t readCRC;
        Serial.readBytes(&readCRC, 1);

        uint8_t calcedCRC = CRC8((const uint8_t *) &port, 2);

        /* If the read IP matches the calculated value, the data is valid */
        if (readCRC == calcedCRC) {
            validRead = true;

            settings_set_mqtt_server_port(port);
        }

    }

    if (validRead) {
        Serial.write(CHAR_MESSAGE_SET_SUCCESS);

    } else {
        Serial.write(CHAR_MESSAGE_SET_FAILURE);
    }
}

/**
 * Sends the MQTT client's IP address out over serial with an identifier and
 * its CRC
 */
void serial_dump_mqtt_client_ip(void)
{
    IPAddress *outIP;

    /* Send the byte indicating the type of message */
    Serial.write(CHAR_TYPE_CLIENT_IP);

    outIP = settings_get_mqtt_client_ip();
    serial_dump_ip(outIP);
}

/**
 * Reads the MQTT clients IP address received via the serial port
 *
 * Order of message
 * Identifier
 * Client IP
 * CRC of IP
 *
 * It is expected that the identifier has already been read
 *
 * This function will respond
 * CHAR_TYPE_CLIENT_IP
 * Result possibilities:
 *  CHAR_MESSAGE_SET_SUCCESS
 *  CHAR_MESSAGE_SET_FAILURE
 */
void serial_load_mqtt_client_ip(void)
{
    IPAddress inIP;

    Serial.write(CHAR_TYPE_CLIENT_IP);

    if (serial_load_ip(&inIP)) {
        settings_set_mqtt_client_ip(&inIP);

        Serial.write(CHAR_MESSAGE_SET_SUCCESS);

    } else {
        Serial.write(CHAR_MESSAGE_SET_FAILURE);
    }
}

/**
 * Sends the WLAN gateway's IP address out over serial with an identifier and
 * its CRC
 */
void serial_dump_wlan_gateway(void)
{
    IPAddress *outIP;

    /* Send the byte indicating the type of message */
    Serial.write(CHAR_TYPE_GATEWAY_IP);

    outIP = settings_get_wlan_gateway();
    serial_dump_ip(outIP);
}

/**
 * Reads the WLAN subnet IP address received via the serial port
 *
 * Order of message
 * Identifier
 * Gateway IP
 * CRC of IP
 *
 * It is expected that the identifier has already been read
 *
 * This function will respond
 * CHAR_TYPE_GATEWAY_IP
 * Result possibilities:
 *  CHAR_MESSAGE_SET_SUCCESS
 *  CHAR_MESSAGE_SET_FAILURE
 */
void serial_load_wlan_gateway(void)
{
    IPAddress inIP;

    Serial.write(CHAR_TYPE_GATEWAY_IP);

    if (serial_load_ip(&inIP)) {
        settings_set_wlan_gateway(&inIP);

        Serial.write(CHAR_MESSAGE_SET_SUCCESS);

    } else {
        Serial.write(CHAR_MESSAGE_SET_FAILURE);
    }
}

/**
 * Sends the WLAN subnet's IP address out over serial with an identifier and
 * its CRC
 */
void serial_dump_wlan_subnet(void)
{
    IPAddress *outIP;

    /* Send the byte indicating the type of message */
    Serial.write(CHAR_TYPE_SUBNET_IP);

    outIP = settings_get_wlan_subnet();
    serial_dump_ip(outIP);
}

/**
 * Reads the WLAN subnet IP address received via the serial port
 *
 * Order of message
 * Identifier
 * Subnet IP
 * CRC of IP
 *
 * It is expected that the identifier has already been read
 *
 * This function will respond
 * CHAR_TYPE_SUBNET_IP
 * Result possibilities:
 *  CHAR_MESSAGE_SET_SUCCESS
 *  CHAR_MESSAGE_SET_FAILURE
 */
void serial_load_wlan_subnet(void)
{
    IPAddress inIP;

    Serial.write(CHAR_TYPE_SUBNET_IP);

    if (serial_load_ip(&inIP)) {
        settings_set_wlan_subnet(&inIP);

        Serial.write(CHAR_MESSAGE_SET_SUCCESS);

    } else {
        Serial.write(CHAR_MESSAGE_SET_FAILURE);
    }
}

/**
 * Sends the WiFi SSID string out over serial
 */
void serial_dump_wifi_ssid(void)
{
    Serial.write(CHAR_TYPE_WIFI_SSID);

    String ssidString = settings_get_wifi_ssid();

    serial_dump_string(&ssidString);
}

/**
 * Reads the WLAN's SSID received via the serial port
 *
 * Order of message
 * Identifier
 * WLAN SSID string
 * CRC of string
 *
 * It is expected that the identifier has already been read
 *
 * This function will respond
 * CHAR_TYPE_WIFI_SSID
 * Result possibilities:
 *  CHAR_MESSAGE_SET_SUCCESS
 *  CHAR_MESSAGE_SET_FAILURE
 */
void serial_load_wifi_ssid(void)
{
    String inString;

    Serial.write(CHAR_TYPE_WIFI_SSID);

    if (serial_load_string(&inString)) {
        settings_set_wifi_ssid((char *)inString.c_str());

        Serial.write(CHAR_MESSAGE_SET_SUCCESS);

    } else {
        Serial.write(CHAR_MESSAGE_SET_FAILURE);
    }
}

/**
 * Sends the WiFi passphrase string out over serial
 */
void serial_dump_wifi_key(void)
{
    Serial.write(CHAR_TYPE_WIFI_PASSPHASE);

    String wifiKey = settings_get_wifi_key();

    serial_dump_string(&wifiKey);
}

/**
 * Reads the WLAN's WiFi key received via the serial port
 *
 * Order of message
 * Identifier
 * WLAN WiFi key string
 * CRC of string
 *
 * It is expected that the identifier has already been read
 *
 * This function will respond
 * CHAR_TYPE_WIFI_PASSPHRASE
 * Result possibilities:
 *  CHAR_MESSAGE_SET_SUCCESS
 *  CHAR_MESSAGE_SET_FAILURE
 */
void serial_load_wifi_key(void)
{
    String inString;

    Serial.write(CHAR_TYPE_WIFI_PASSPHASE);

    if (serial_load_string(&inString)) {
        settings_set_wifi_key((char *) inString.c_str());

        Serial.write(CHAR_MESSAGE_SET_SUCCESS);

    } else {
        Serial.write(CHAR_MESSAGE_SET_FAILURE);
    }
}

/**
 * Sends the device name string out over serial
 */
void serial_dump_device_name(void)
{
    Serial.write(CHAR_TYPE_DEVICE_NAME);
    
    String deviceName = settings_get_device_name();

    serial_dump_string(&deviceName);
}

/**
 * Reads the device name received via the serial port
 *
 * Order of message
 * Identifier
 * Device name string
 * CRC of string
 *
 * It is expected that the identifier has already been read
 * 
 * Test String TestDevice
 * $73 $F7 $0A $54 $65 $73 $74 $44 $65 $76 $69 $63 $65 $25 $E0
 *
 * This function will respond
 * CHAR_TYPE_DEVICE_NAME
 * Result possibilities:
 *  CHAR_MESSAGE_SET_SUCCESS
 *  CHAR_MESSAGE_SET_FAILURE
 */
void serial_load_device_name(void)
{
    String inString;

    Serial.write(CHAR_TYPE_DEVICE_NAME);

    if (serial_load_string(&inString)) {
        settings_set_device_name((char *) inString.c_str());

        Serial.write(CHAR_MESSAGE_SET_SUCCESS);

    } else {
        Serial.write(CHAR_MESSAGE_SET_FAILURE);
    }
}

/**
 * Sends the MQTT topic prefix out over serial
 */
void serial_dump_mqtt_topic_prefix(void)
{
    Serial.write(CHAR_TYPE_MQTT_PREFIX);
    
    String mqttPrefix = settings_get_mqtt_topic_prefix();

    serial_dump_string(&mqttPrefix);
}

/**
 * Reads the MQTT topic received via the serial port
 *
 * Order of message
 * Identifier
 * MQTT topic prefix string
 * CRC of string
 *
 * It is expected that the identifier has already been read
 * 
 * Test String "homeassistant"
 *
 * This function will respond
 * CHAR_TYPE_MQTT_PREFIX
 * Result possibilities:
 *  CHAR_MESSAGE_SET_SUCCESS
 *  CHAR_MESSAGE_SET_FAILURE
 */
void serial_load_mqtt_topic_prefix(void)
{
    String inString;

    Serial.write(CHAR_TYPE_MQTT_PREFIX);

    if (serial_load_string(&inString)) {
        settings_set_mqtt_topic_prefix((char *) inString.c_str());

        Serial.write(CHAR_MESSAGE_SET_SUCCESS);

    } else {
        Serial.write(CHAR_MESSAGE_SET_FAILURE);
    }
}

/**
 * Sends the time between sensor reads (in seconds) out over serial
 * 
 * This function will output
 * CHAR_TYPE_SLEEP_TIME
 * Time in seconds (LSb time in first byte, MSb in second)
 * CRC of sleep time
 */
void serial_dump_sleep_time(void)
{
    uint16_t sleepTime = settings_get_sleep_time();
    Serial.write(CHAR_TYPE_SLEEP_TIME);

    Serial.write(sleepTime & 0xFF);
    Serial.write(sleepTime >> 8);

    Serial.write(CRC8((uint8_t *) &sleepTime, 2));
}

/**
 * Reads the sleep received via the serial port
 *
 * Order of message
 * CHAR_TYPE_SLEEP_TIME
 * Time in seconds (LSb time in first byte, MSb in second)
 * CRC of sleep time
 *
 * It is expected that the tikme has already been read
 *
 * This function will respond
 * CHAR_TYPE_SLEEP_TIME
 * Result possibilities:
 *  CHAR_MESSAGE_SET_SUCCESS
 *  CHAR_MESSAGE_SET_FAILURE
 */
void serial_load_sleep_time(void)
{
    bool validRead = false;
    uint16_t time;

    Serial.write(CHAR_TYPE_SLEEP_TIME);

    if (Serial.readBytes((uint8_t *) &time, 2) == 2) {
        uint8_t readCRC;
        Serial.readBytes(&readCRC, 1);

        uint8_t calcedCRC = CRC8((const uint8_t *) &time, 2);

        /* If the read CRC matches the calculated value, the data is valid */
        if (readCRC == calcedCRC) {
            validRead = true;

            settings_set_sleep_time(time);
        }

    }

    if (validRead) {
        Serial.write(CHAR_MESSAGE_SET_SUCCESS);

    } else {
        Serial.write(CHAR_MESSAGE_SET_FAILURE);
    }
}

/**
 * Sends the given IP address as a series of 4 bytes plus a 5th, CRC byte
 */
void serial_dump_ip(IPAddress *ipToDump)
{
    uint8_t localIP[IP_BYTE_COUNT];

    /* Send each IP byte */
    for (uint8_t i = 0; i < IP_BYTE_COUNT; i++) {
        localIP[i] = ipToDump->operator[](i);
        Serial.write(localIP[i]);
    }

    /* Calculate and send the CRC */
    uint8_t calcedCRC;
    calcedCRC = CRC8((const uint8_t *) localIP, IP_BYTE_COUNT);
    Serial.write(calcedCRC);
}

/**
 * Reads an IP address off of the serial bus
 *
 * If the read value is a valid IP address, ipRead will be updated and true
 * will be returned
 *
 * @param ipRead Location to store valid IP addresses
 * @return True if a valid IP address was read and ipRead was updated
 */
bool serial_load_ip(IPAddress *ipRead)
{
    bool validRead = false;
    uint8_t localIP[IP_BYTE_COUNT];

    /* Read the IP. If IP_BYTE_COUNT bytes were read, continue */
    if (Serial.readBytes(localIP, IP_BYTE_COUNT) == IP_BYTE_COUNT) {

        uint8_t readCRC;
        Serial.readBytes(&readCRC, 1);

        uint8_t calcedCRC = CRC8((const uint8_t *) localIP, IP_BYTE_COUNT);

        /* If the read IP matches the calculated value, the data is valid */
        if (readCRC == calcedCRC) {
            validRead = true;

            *ipRead = IPAddress(localIP[0], localIP[1], localIP[2], localIP[3]);
        }
    }

    return validRead;
}

/**
 * Sends the given string as the length, then, a series of bytes, and
 * finally a CRC byte
 */
void serial_dump_string(String *inString)
{
    unsigned int inStrLen = inString->length();

    Serial.write(inStrLen);

    for (uint8_t i = 0; i < inStrLen; i++) {
        Serial.write(inString->operator[](i));
    }

    uint8_t calcedCRC;
    calcedCRC = CRC8((const uint8_t *) &(inString->operator[](0)), inStrLen);
    Serial.write(calcedCRC);
}

/**
 * Receive and validate a string sent over serial
 *
 * @param inString Location to store the read string
 * @return True, if the value was valid and inString has been updated
 */
bool serial_load_string(String *inString)
{
    bool valid = false;
    uint8_t bytesExpected = 0;
    uint8_t bytesLimited = 0;
    uint8_t readData[MAX_SERIAL_STRING_LEN + 1]; /* Add one char for the null char */
    uint8_t readCRC;

    Serial.readBytes(&bytesExpected, 1);

    if (bytesExpected > MAX_SERIAL_STRING_LEN) {
        bytesLimited = MAX_SERIAL_STRING_LEN;
    }

    /* If the expected number of characters were read contiue */
    if (Serial.readBytes(readData, bytesExpected) == bytesExpected) {

        /* If there were too many character to read burn the remaining,
        characters and the checksum */
        if (false) {//(bytesLimited < bytesExpected) {
            bytesExpected -= bytesLimited;
            bytesExpected++; /* Don't forget the checksum */

            while((bytesExpected > 0) && Serial.available()) {
                uint8_t burn;
                Serial.readBytes(&burn, 1);
                bytesExpected--;
            }

        } else {

            /* Null terminate the string */
            readData[bytesExpected] = 0;

            /* Get the expected CRC and compare it to the expected. If they match
            * update the value */
            Serial.readBytes(&readCRC, 1);

            uint8_t calcCRC;
            calcCRC = CRC8((const uint8_t*) readData, bytesExpected);

            if (readCRC == calcCRC) {
                *inString = String((const char *) readData);
                valid = true;
            }
        }
    }


    return valid;
}