#include <Arduino.h>
#include <ESP8266WiFi.h>
#include "wifi_client.h"
#include "settings.h"

/**
 * Connects to the WiFi AP with a static IP
 *
 * Will not check if a connection is established before exiting
 */
void init_wifi(void)
{
    char *ssid = settings_get_wifi_ssid();
    char *pass = settings_get_wifi_key();
    IPAddress clientIP = *settings_get_mqtt_client_ip();
    IPAddress gateway = *settings_get_wlan_gateway();
    IPAddress subnet = *settings_get_wlan_subnet();

    Serial.printf("Connecting to %s\r\n", ssid);
    
    WiFi.persistent(false);
    WiFi.config(clientIP, gateway, subnet);
    WiFi.begin(ssid, pass);
}

/**
 * Delays until a WiFi connection is established
 * 
 * @param maxWait Maximum time to wait. If 0 is given, there will be no timeout
 * and the function will wait until a connection is made
 * 
 * @return A connection was made before timing out
 */
bool wifi_wait_for_connection(unsigned long maxWait)
{
    bool checkWait;
    bool timedOut = false;
    unsigned long startTime;

    startTime = millis();
    checkWait = (maxWait > 0);

    /* Loop until a connection is made or the system times out */
    while (WiFi.status() != WL_CONNECTED) {
        if (!checkWait || (millis() - startTime < maxWait)) {
            delay(10);
        } else {
            timedOut = true;
            break;
        }
    }

    if (timedOut) {
        Serial.println("Timed outing waiting for WiFi connection");
    } else {
        Serial.println("WiFi connected");
        Serial.print("IP address: ");
        Serial.println(WiFi.localIP());
    }

    return !timedOut;
}