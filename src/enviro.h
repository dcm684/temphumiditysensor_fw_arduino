#ifndef ENVIRO_H
#define ENVIRO_H

#define DEF_DHT 0xFFF0
#define DEF_HTU21 0xFFF1
#define DEF_BME280 0xFFF2

// #define DEFAULT_SENSOR DEF_DHT
// #define DEFAULT_SENSOR DEF_HTU21
#define DEFAULT_SENSOR DEF_BME280

void DHT_printEnviroResults(void);
void DHT_readEnviroSensor(void);
void DHT_initEnviroSensor(void);
float DHT_getTemperatureCelsius(void);
float DHT_getHumidity(void);

void HTU21D_printEnviroResults(void);
void HTU21D_readEnviroSensor(void);
void HTU21D_initEnviroSensor(void);
float HTU21D_getTemperatureCelsius(void);
float HTU21D_getHumidity(void);

void BME280_printEnviroResults(void);
void BME280_readEnviroSensor(void);
void BME280_initEnviroSensor(void);
float BME280_getTemperatureCelsius(void);
float BME280_getHumidity(void);

#if (DEFAULT_SENSOR == DEF_DHT)
#define enviro_printEnviroResults DHT_printEnviroResults
#define enviro_readEnviroSensor DHT_readEnviroSensor
#define enviro_initEnviroSensor DHT_initEnviroSensor
#define enviro_getTemperatureCelsius DHT_getTemperatureCelsius
#define enviro_getHumidity DHT_getHumidity

#elif (DEFAULT_SENSOR == DEF_BME280)
#define enviro_printEnviroResults BME280_printEnviroResults
#define enviro_readEnviroSensor BME280_readEnviroSensor
#define enviro_initEnviroSensor BME280_initEnviroSensor
#define enviro_getTemperatureCelsius BME280_getTemperatureCelsius
#define enviro_getHumidity BME280_getHumidity

#elif (DEFAULT_SENSOR == DEF_HTU21)
#define enviro_printEnviroResults HTU21D_printEnviroResults
#define enviro_readEnviroSensor HTU21D_readEnviroSensor
#define enviro_initEnviroSensor HTU21D_initEnviroSensor
#define enviro_getTemperatureCelsius HTU21D_getTemperatureCelsius
#define enviro_getHumidity HTU21D_getHumidity

#else
#error No default sensor defined
#endif

#define ENVIRO_PRINT_STR_TEMPC_HUMID "Temp: %6.2f°C Humid: %6.2f%%\n"

#endif /* ENVIRO_H */