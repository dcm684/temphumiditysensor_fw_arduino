#include "settings.h"

#define MAX_SERIAL_STRING_LEN 64

#if (MAX_SSID_LEN > MAX_SERIAL_STRING_LEN) || (MAX_WIFI_KEY_LEN > MAX_SERIAL_STRING_LEN)
#error MAX_SERIAL_STRING_LEN is too small
#endif /* MAX_SERIAL_STRING_LEN validation */

void init_serial(void);
void check_and_process_serial(void);
void serial_dump_all_settings(void);
void serial_dump_mqtt_server_ip(void);
void serial_dump_mqtt_server_port(void);
void serial_dump_mqtt_client_ip(void);
void serial_dump_wlan_gateway(void);
void serial_dump_wlan_subnet(void);
void serial_dump_wifi_ssid(void);
void serial_dump_wifi_key(void);
void serial_dump_device_name(void);