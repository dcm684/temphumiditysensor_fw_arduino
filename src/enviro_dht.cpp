#include <DHT.h>

#include "enviro.h"

DHT theDHT;
const uint8_t dhtPin = D6;
static unsigned long nextReadTime;

/**
 * Indicate that another read can occur in getMinimumSamplingPeriod() ms from now
 */
void DHT_updateNextReadTime(void)
{
    nextReadTime = millis() + theDHT.getMinimumSamplingPeriod();
}

/**
 * Has enough time passed to allow another sensor read?
 * 
 * @return True if enough time has passed
 */
bool DHT_canReadAgain(void)
{
    return (millis() >= nextReadTime);
}

void DHT_initEnviroSensor(void)
{
    theDHT.setup(dhtPin);
    DHT_updateNextReadTime();
}

float DHT_getHumidity(void)
{
    return theDHT.getHumidity();
}

float DHT_getTemperatureCelsius(void)
{
    return theDHT.getTemperature();
}

void DHT_printEnviroResults(void) {
    Serial.printf(ENVIRO_PRINT_STR_TEMPC_HUMID, 
                    DHT_getTemperatureCelsius(),
                    DHT_getHumidity());
}

void DHT_readEnviroSensor(void) {
    while(!DHT_canReadAgain());

    DHT_printEnviroResults();

    DHT_updateNextReadTime();
}