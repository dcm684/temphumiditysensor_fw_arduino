#include "HTU21D.h"

#include "enviro.h"

HTU21D theHTU21D(HTU21D_RES_RH12_TEMP14);

/**
 * Initialized the HTU21D sensor
 */
void HTU21D_initEnviroSensor(void)
{
    if (!theHTU21D.begin()) {
        Serial.print("Failed to init HTU21D\r\n");
    }
}

/**
 * Returns the temperature in Celsius
 */
float HTU21D_getTemperatureCelsius(void)
{
    return theHTU21D.readTemperature(HTU21D_TRIGGER_TEMP_MEASURE_HOLD);
}

/**
 * Returns the temperature compenstated humidity
 */
float HTU21D_getHumidity(void)
{
    return theHTU21D.readCompensatedHumidity(HTU21D_FORCE_READ_TEMP);
}
/**
 * Prints the temperature (in °C) and humidity (in %)
 */
void HTU21D_printEnviroResults(void)
{
    Serial.printf(ENVIRO_PRINT_STR_TEMPC_HUMID, 
                    HTU21D_getTemperatureCelsius(),
                    HTU21D_getHumidity());
}

/**
 * Simply calls printEnviroResults()
 * 
 * SHT21D does not need the extra delay like DHT needs so this function is
 * redundant
 */
void HTU21D_readEnviroSensor(void)
{
    HTU21D_printEnviroResults();
}
