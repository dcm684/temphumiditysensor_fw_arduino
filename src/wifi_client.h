#ifndef WIFI_CLIENT_H
#define WIFI_CLIENT_H

#include <ESP8266WiFi.h>

void init_wifi(void);
bool wifi_wait_for_connection(unsigned long maxWait);

#endif /* WIFI_CLIENT_H */