#include <stdbool.h>
#include <EEPROM.h>
//#include <EEPROM_rotate.h>
#include <ESP8266WiFi.h>

#include "settings.h"
#include "math_crc.h"

// extern EEPROM_Rotate persistentStorage;
extern EEPROMClass persistentStorage;

IPAddress serverAddress(192, 168, 1, 80);
IPAddress clientAddress(192, 168, 1, 201);
IPAddress wlanGateway(192, 168, 1, 1);
IPAddress wlanSubnet(255, 255, 255, 0);

uint16_t serverPort = 1883;
uint16_t secondsBetweenReads = 600;

char wifiSSID[MAX_SSID_LEN] = "OscarMayerBologna";
char wifiKey[MAX_WIFI_KEY_LEN] = "We are the mighty mighty Meyers!";
char deviceName[MAX_DEVICE_NAME_LEN] = {0}; /* When empty defaults to full mac address as a string */
char DEFAULT_DEVICE_NAME_FORMAT[] PROGMEM = "ESP8266_%02X%02X%02X%02X%02X%02X";
char mqttPrefix[MAX_MQTT_PREFIX_LEN] = "sensor";

enum StatusBits {
    STATUS_SERVER_IP,
    STATUS_CLIENT_IP,
    STATUS_SSID_AND_KEY,
    STATUS_WLAN_GATEWAY_AND_SUBNET,
    STATUS_DEVICE_NAME,
    STATUS_MQTT_PREFIX,
    STATUS_SLEEP_TIME,
    STATUS_BIT_COUNT
};

#define ADDR_STATUS 0
#define ADDR_STATUS_CHECKSUM (ADDR_STATUS + 1)
#define ADDR_RANDOM_VALUE (ADDR_STATUS_CHECKSUM + 1)
#define ADDR_SERVER_IP_0 (ADDR_RANDOM_VALUE + 2)
#define ADDR_SERVER_PORT (ADDR_SERVER_IP_0 + IP_BYTE_COUNT)
#define ADDR_CLIENT_IP_0 (ADDR_SERVER_PORT + sizeof(serverPort))
#define ADDR_GATEWAY_IP_0 (ADDR_CLIENT_IP_0 + IP_BYTE_COUNT)
#define ADDR_SUBNET_IP_0 (ADDR_GATEWAY_IP_0 + IP_BYTE_COUNT)

#define ADDR_SSID_CHAR_0 (ADDR_SUBNET_IP_0 + IP_BYTE_COUNT)
#define ADDR_WIFI_KEY_CHAR_0 (ADDR_SSID_CHAR_0 + MAX_SSID_LEN)

#define ADDR_DEVICE_NAME_0 (ADDR_WIFI_KEY_CHAR_0 + MAX_WIFI_KEY_LEN)
#define ADDR_MQTT_PREFIX_0 (ADDR_DEVICE_NAME_0 + MAX_DEVICE_NAME_LEN)

#define ADDR_SLEEP_SECONDS (ADDR_MQTT_PREFIX_0 + MAX_MQTT_PREFIX_LEN)

bool setting_status_is_valid(void);
void reset_status_byte(void);
uint8_t get_expected_status_checksum(uint8_t status);

void settings_load(void);

bool load_ip(uint eepromAddress, IPAddress *destination);
bool store_ip(uint eepromAddress, IPAddress *destination);
bool load_string(unsigned int stringAddress, char *source, uint length);
bool store_string(unsigned int stringAddress, char *source, uint length);

void load_mqtt_server_ip_and_port(void);
void store_mqtt_server_ip_and_port(void);
void load_mqtt_client_ip(void);
void store_mqtt_client_ip(void);
void load_wlan_subnet_and_gateway_ips(void);
void store_wlan_subnet_and_gateway_ips(void);

void load_wifi_ssid_and_key(void);
void store_wifi_ssid_and_key(void);

void load_device_name(void);
void store_device_name(void);

void load_mqtt_topic_prefix(void);
void store_mqtt_topic_prefix(void);

void load_sleep_time(void);
void store_sleep_time(void);

/**
 * Get a semi-unique value based on the CRC16 of the compile date/time
 *
 * @return Unique value based on compile time
 */
uint16_t compile_unique_value_generate(void)
{
    const char compile_date[] = __DATE__ " " __TIME__;
    uint16_t crcResult;

    crcResult = CRC16((const uint8_t*) compile_date,
                      sizeof(compile_date)/sizeof(char));

    return crcResult;
}

/**
 * Stores a unique value to EEPROM
 */
void compile_unique_value_store(void)
{
    uint16_t uniqueValue;
    uniqueValue = compile_unique_value_generate();

    persistentStorage.put(ADDR_RANDOM_VALUE, &uniqueValue);
}

/**
 * Returns if the stored unique value matches the expected unique value
 *
 * Since it is based on the compile time, this function will return false after
 * compiling until a new value is stored
 */
bool compile_unique_value_stored_value_is_valid(void)
{
    uint16_t storedUniqueValue;

    persistentStorage.get(ADDR_RANDOM_VALUE, storedUniqueValue);

    return storedUniqueValue == compile_unique_value_generate();
}

/**
 * Returns true if the status byte is valid
 *
 * Byte is valid when it is the inverse of its checksum byte
 */
bool setting_status_is_valid(void)
{
    uint8_t statusValue;
    uint8_t checksum;

    statusValue = persistentStorage.read(ADDR_STATUS);
    checksum = persistentStorage.read(ADDR_STATUS_CHECKSUM);

    return (checksum == get_expected_status_checksum(statusValue));
}

/**
 * Resets all status bits to the off state.
 *
 * Sets the status checksum to its expected value
 */
void reset_status_byte(void)
{
    uint8_t statusValue = 0;

    persistentStorage.write(ADDR_STATUS, statusValue);
    persistentStorage.write(ADDR_STATUS_CHECKSUM,
                            get_expected_status_checksum(statusValue));
}

/**
 * Calculates the expected status checksum value for the given status byte
 *
 * @param status Status value to calculate CS from
 * @param Calculate checksum
 */
uint8_t get_expected_status_checksum(uint8_t status) {
    return ~status;
}

/**
 * Checks if the given setting is set
 * @param settingType Setting to check
 * @return True if the setting is set
 */
bool is_settings_set(StatusBits settingType)
{
    if (settingType >= STATUS_BIT_COUNT) {
        return false;
    }

    uint8_t statusByte;


    statusByte = persistentStorage.read(ADDR_STATUS);


    return bitRead(statusByte, settingType);
}

/**
 * Indicates that the given setting is stored
 *
 * Expects that the current status byte is valid
 */
void indicate_is_settings_set(StatusBits settingType)
{
    if (settingType < STATUS_BIT_COUNT) {
        uint8_t statusTemp;

        statusTemp = persistentStorage.read(ADDR_STATUS);


        bitSet(statusTemp, settingType);


        persistentStorage.write(ADDR_STATUS, statusTemp);
        persistentStorage.write(ADDR_STATUS_CHECKSUM,
                                get_expected_status_checksum(statusTemp));
    }
}

/**
 * Initializes the settings module
 *
 * If the settings are invalid, the persistant storeage will be initialized,
 * otherwise values will be loaded from persistent storage
 */
void init_settings(void) {

    if (setting_status_is_valid()) {
        settings_load();
    } else {
        reset_status_byte();
    }
}

/**
 * Load the device settings from persistant storage
 */
void settings_load(void)
{
    load_mqtt_server_ip_and_port();
    load_mqtt_client_ip();
    load_wlan_subnet_and_gateway_ips();
    load_wifi_ssid_and_key();
    load_device_name();
    load_mqtt_topic_prefix();
    load_sleep_time();
}

/**
 * Load the IP address stored at the given location
 *
 * If the eepromAddress is invalid, the value at destination will not change
 *
 * @param eepromAddress Address to load the IP from
 * @param destination Location to store the IP at
 * @return True if the IPAddress was loaded
 */
bool load_ip(unsigned int eepromAddress, IPAddress *destination)
{
    bool validAddress = false;
    if (eepromAddress < persistentStorage.length() - IP_BYTE_COUNT) {

        uint32_t ipAddr;
        persistentStorage.get(eepromAddress, ipAddr);

        *destination = IPAddress(ipAddr);

        validAddress = true;
    }

    return validAddress;
}

/**
 * Store the given IP at the given address
 *
 * If the eepromAddress is invalid, nothing will be stored
 *
 * DOES NOT COMMIT TO PERSISTENT STORAGE!
 *
 * @param eepromAddress Address to store the IP at
 * @return True, if the IPAddress was stored
 */
bool store_ip(unsigned int eepromAddress, IPAddress *source)
{
    bool validAddress = false;
    if (eepromAddress < persistentStorage.length() - IP_BYTE_COUNT) {

        uint32_t ipAddr = *source;
        persistentStorage.put(eepromAddress, (uint32_t) ipAddr);
        // for (uint8_t i = 0; i < 4; i++) {
        //     persistentStorage.write(eepromAddress + i, source[i]);
        // }

        validAddress = true;
    }

    return validAddress;
}

/**
 * Loads a string from the given address in EEPROM
 *
 * Data stored must end in 0x00
 *
 * @param stringAddress Address in EEPROM to load the string fromt
 * @param destination Where to stored the loaded string
 * @param length Maximum number of characters to load. If a byte of 0x00 is
 * loaded, no further bytes will be loaded
 * @return True, if the data was loaded and the final character is 0x00
 */
bool load_string(unsigned int stringAddress, char *destination, uint length)
{
    bool validMessage = false;

    if (stringAddress + length <= persistentStorage.length()) {

        for (uint i = 0; i < length; i++) {
            destination[i] = persistentStorage.read(stringAddress + i);
            if (destination[i] == 0) {
                validMessage = true;
                break;
            }
        }
    }

    return validMessage;
}

/**
 * Loads a string from the given address in EEPROM
 *
 * Data stored must end in 0x00
 *
 * DOES NOT COMMIT TO PERSISTENT STORAGE!
 *
 * @param stringAddress Address in EEPROM to store the string fromt
 * @param source Where to stored the stored string
 * @param length Maximum number of characters to store. If a byte of 0x00 is
 * stored, no further bytes will be stored
 * @return True, if the data was stored and the final character is 0x00
 */
bool store_string(unsigned int stringAddress, char *source, uint length)
{
    bool validMessage = false;

    if (stringAddress + length <= persistentStorage.length()) {

        for (uint i = 0; i < length; i++) {
            persistentStorage.write(stringAddress + i, source[i]);
            if (source[i] == 0) {
                validMessage = true;
                break;
            }
        }
    }

    return validMessage;
}

/**
 * If a server IP is stored in settings, it will get loaded
 */
void load_mqtt_server_ip_and_port(void)
{
    if (is_settings_set(STATUS_SERVER_IP)) {
        load_ip(ADDR_SERVER_IP_0, &serverAddress);
        persistentStorage.get(ADDR_SERVER_PORT, serverPort);
    }
}

/**
 * Store the current serverAddress in EEPROM
 *
 * DOES NOT COMMIT TO PERSISTENT STORAGE!
 */
void store_mqtt_server_ip_and_port(void)
{
    store_ip(ADDR_SERVER_IP_0, &serverAddress);
    persistentStorage.put(ADDR_SERVER_PORT, serverPort);

    indicate_is_settings_set(STATUS_SERVER_IP);
}

/**
 * If a client IP is stored in settings, it will get loaded
 */
void load_mqtt_client_ip(void)
{
    if (is_settings_set(STATUS_CLIENT_IP)) {
        load_ip(ADDR_CLIENT_IP_0, &clientAddress);
    }
}

/**
 * Store the current clientAddress in EEPROM
 *
 * DOES NOT COMMIT TO PERSISTENT STORAGE!
 */
void store_mqtt_client_ip(void)
{
    store_ip(ADDR_CLIENT_IP_0, &clientAddress);
    indicate_is_settings_set(STATUS_CLIENT_IP);
}

/**
 * If a subnet and gateway IPs are stored in settings, they will get loaded
 */
void load_wlan_subnet_and_gateway_ips(void)
{
    if (is_settings_set(STATUS_WLAN_GATEWAY_AND_SUBNET)) {
        load_ip(ADDR_GATEWAY_IP_0, &wlanGateway);
        load_ip(ADDR_SUBNET_IP_0, &wlanSubnet);
    }
}

/**
 * Store the current subnet and gateway IPs in EEPROM
 *
 * DOES NOT COMMIT TO PERSISTENT STORAGE!
 */
void store_wlan_subnet_and_gateway_ips(void)
{
    store_ip(ADDR_GATEWAY_IP_0, &wlanGateway);
    store_ip(ADDR_SUBNET_IP_0, &wlanSubnet);
    indicate_is_settings_set(STATUS_WLAN_GATEWAY_AND_SUBNET);
}

/**
 * If the WiFi SSID and Key are stored in EEPROM, load them
 */
void load_wifi_ssid_and_key(void)
{
    if (is_settings_set(STATUS_SSID_AND_KEY)) {
        load_string(ADDR_SSID_CHAR_0, wifiSSID, MAX_SSID_LEN);
        load_string(ADDR_WIFI_KEY_CHAR_0, wifiKey, MAX_WIFI_KEY_LEN);
    }
}

/**
 * Store the WiFi SSID and key in EEPROM
 *
 * DOES NOT COMMIT TO PERSISTENT STORAGE!
 */
void store_wifi_ssid_and_key(void)
{
    store_string(ADDR_SSID_CHAR_0, wifiSSID, MAX_SSID_LEN);
    store_string(ADDR_WIFI_KEY_CHAR_0, wifiKey, MAX_WIFI_KEY_LEN);
    indicate_is_settings_set(STATUS_SSID_AND_KEY);
}

/**
 * Loads the device name stored in EEPROM
 */
void load_device_name(void) {
    if (is_settings_set(STATUS_DEVICE_NAME)) {
        load_string(ADDR_DEVICE_NAME_0, deviceName, MAX_DEVICE_NAME_LEN);
    } else {
        settings_set_device_name_to_unique_name();
    }
}

/**
 * Store the device name in EEPROM
 *
 * DOES NOT COMMIT TO PERSISTENT STORAGE!
 */
void store_device_name(void)
{
    store_string(ADDR_DEVICE_NAME_0, deviceName, MAX_DEVICE_NAME_LEN);
    indicate_is_settings_set(STATUS_DEVICE_NAME);
}

/**
 * Loads the MQTT prefix stored in EEPROM.
 */
void load_mqtt_topic_prefix(void) {
    if (is_settings_set(STATUS_MQTT_PREFIX)) {
        load_string(ADDR_MQTT_PREFIX_0, mqttPrefix, MAX_MQTT_PREFIX_LEN);
    }
}

/**
 * Store the MQTT prefix in EEPROM
 *
 * DOES NOT COMMIT TO PERSISTENT STORAGE!
 */
void store_mqtt_topic_prefix(void)
{
    store_string(ADDR_MQTT_PREFIX_0, mqttPrefix, MAX_MQTT_PREFIX_LEN);
    indicate_is_settings_set(STATUS_MQTT_PREFIX);
}

/**
 * Load the sleep time from EEPROM
 */
void load_sleep_time(void)
{
    if (is_settings_set(STATUS_SLEEP_TIME)) {
        persistentStorage.get(ADDR_SLEEP_SECONDS, secondsBetweenReads);
    }
}

/**
 * Store the sleep time in EEPROM
 *
 * DOES NOT COMMIT TO PERSISTENT STORAGE!
 */
void store_sleep_time(void)
{
    persistentStorage.put(ADDR_SLEEP_SECONDS, secondsBetweenReads);
    indicate_is_settings_set(STATUS_SLEEP_TIME);
}



/**
 * Return the current MQTT server IPAddress
 *
 * @return MQTT server's address
 */
IPAddress* settings_get_mqtt_server_ip(void)
{
    return &serverAddress;
}

/**
 * Set the MQTT server IPAddress and store the value persistantly
 *
 * DOES NOT COMMIT TO PERSISTENT STORAGE!
 *
 * @param inAddr IPAddress for the desired MQTT server
 */
void settings_set_mqtt_server_ip(IPAddress *inAddr)
{
    memcpy(&serverAddress, inAddr, sizeof(IPAddress));

    store_mqtt_server_ip_and_port();
}

/**
 * Return the current MQTT client IPAddress
 *
 * @return This module's IPAddress
 */
IPAddress* settings_get_mqtt_client_ip(void)
{
    return &clientAddress;
}

/**
 * Set the MQTT client IPAddress and store the value persistantly
 *
 * DOES NOT COMMIT TO PERSISTENT STORAGE!
 *
 * @param inAddr IPAddress for the desired MQTT client (this module)
 */
void settings_set_mqtt_client_ip(IPAddress *inAddr)
{
    memcpy(&clientAddress, inAddr, sizeof(IPAddress));
    store_mqtt_client_ip();
}

/**
 * Return the current MQTT server port number
 *
 * @return Current MQTT server port number
 */
uint16_t settings_get_mqtt_server_port(void)
{
    return serverPort;
}

/**
 * Set the MQTT server port number and store the value persistantly
 *
 * DOES NOT COMMIT TO PERSISTENT STORAGE!
 *
 * @param inPort Desired port for MQTT server
 */
void settings_set_mqtt_server_port(uint16_t inPort)
{
    if (inPort < MAX_PORT_NUMBER) {
        serverPort = inPort;
        store_mqtt_server_ip_and_port();
    }
}

/**
 * Return the current WLAN SSID
 *
 * @return Current WLAN SSID
 */
char* settings_get_wifi_ssid(void)
{
    return wifiSSID;
}

/**
 * Set the WLAN SSID and store the value persistantly
 *
 * DOES NOT COMMIT TO PERSISTENT STORAGE!
 *
 * @param inSSID SSID for WLAN gateway
 */
void settings_set_wifi_ssid(char *inSSID)
{
    if (strlen(inSSID) <= MAX_SSID_LEN) {
        strlcpy((char*) &wifiSSID, (const char*) inSSID, MAX_SSID_LEN);
        store_wifi_ssid_and_key();
    }
}

/**
 * Return the current WLAN key
 *
 * @return Current WLAN key
 */
char* settings_get_wifi_key(void)
{
    return wifiKey;
}

/**
 * Set the WLAN key and store the value persistantly
 *
 * DOES NOT COMMIT TO PERSISTENT STORAGE!
 *
 * @param inKey WLAN key
 */
void settings_set_wifi_key(char* inKey)
{
    if (strlen(inKey) <= MAX_WIFI_KEY_LEN) {
        strlcpy((char *) wifiKey, inKey, MAX_WIFI_KEY_LEN);
        store_wifi_ssid_and_key();
    }
}

/**
 * Return the current WLAN gateway IPAddress
 *
 * @return IPAddress for WLAN gateway
 */
IPAddress* settings_get_wlan_gateway(void)
{
    return &wlanGateway;
}

/**
 * Set the WLAN gateway IPAddress
 *
 * DOES NOT COMMIT TO PERSISTENT STORAGE!
 *
 * @param inAddr IPAddress for the desired WLAN gateway
 */
void settings_set_wlan_gateway(IPAddress *inAddr)
{
    memcpy(&wlanGateway, inAddr, sizeof(IPAddress));
    store_wlan_subnet_and_gateway_ips();
}

/**
 * Return the current WLAN subnet IPAddress
 *
 * @return Current WLAN subnet IPAddress
 */
IPAddress* settings_get_wlan_subnet(void)
{
    return &wlanSubnet;
}

/**
 * Set the WLAN subnet IPAddress
 *
 * DOES NOT COMMIT TO PERSISTENT STORAGE!
 *
 * @param inAddr IPAddress for the desired WLAN subnet
 */
void settings_set_wlan_subnet(IPAddress *inAddr)
{
    memcpy(&wlanSubnet, inAddr, sizeof(IPAddress));
    store_wlan_subnet_and_gateway_ips();
}

/**
 * Return the current device name
 *
 * @return Current device name
 */
char* settings_get_device_name(void)
{
    char *outStr;
    outStr = deviceName;
    return outStr;
}

/**
 * Set and store the device name.
 *
 * STORES BUT DOES NOT COMMIT TO PERSISTANT STORAGE!
 *
 * @param inDeviceName New name for the device. If the string length is 0, i.e.
 *                      byte 0 = 0, the name will be based on the MAC ADDRESS.
 *                      In this case the name stored in EEPROM will be 0. Name
 *                      length must be less than or equal to
 *                      MAX_DEVICE_NAME_LEN. This includes the null char
 */
void settings_set_device_name(char *inDeviceName)
{
    if (strlen(inDeviceName) <= MAX_DEVICE_NAME_LEN) {

        strlcpy(deviceName, inDeviceName, MAX_DEVICE_NAME_LEN);
        store_device_name();

        /* If no name is specfied generate a unique name based on the MAC */
        if (inDeviceName[0] == 0) {

            settings_set_device_name_to_unique_name();
        }
    }

}

/**
 * Set deviceName to a unique name based on the ESP8266 MAC
 */
void settings_set_device_name_to_unique_name(void)
{
    byte mac[6];
    WiFi.macAddress(mac);

    sprintf_P(deviceName, DEFAULT_DEVICE_NAME_FORMAT,
              mac[5], mac[4], mac[3], mac[2], mac[1], mac[0]);
}

/**
 * Return the current device name
 *
 * This is the portion of the text right up to the '/' before the device name
 *
 * @return Current device name
 */
char* settings_get_mqtt_topic_prefix(void)
{
    char *outStr;
    outStr = mqttPrefix;
    return outStr;
}

/**
 * Set and store the MQTT prefix.
 *
 * This is the portion of the text right up to the '/' before the device name.
 *
 * If a '/' is the last char it will be stripped.
 *
 * If a null string or lone '\' is received, nothing will change
 *
 * STORES BUT DOES NOT COMMIT TO PERSISTANT STORAGE!
 *
 * @param inMQTTPrefix New MQTT prefix for the device
 *                      Name length must be less than or equal to
 *                      MAX_MQTT_PREFIX_LEN. This includes the null char.
 */
void settings_set_mqtt_topic_prefix(char *inMQTTPrefix)
{
    /* Strip off a trailing '\' */
    if ((strlen(inMQTTPrefix) > 0)
            && inMQTTPrefix[strlen(inMQTTPrefix - 1)] == '\\') {
        inMQTTPrefix[strlen(inMQTTPrefix - 1)] = 0;
    }

    if ((strlen(inMQTTPrefix) <= MAX_MQTT_PREFIX_LEN)
            && (inMQTTPrefix[0] != 0)) {

        strlcpy(mqttPrefix, inMQTTPrefix, MAX_MQTT_PREFIX_LEN);
        store_mqtt_topic_prefix();
    }

}

/**
 * Return the time (in seconds) between readings
 *
 * @return Time in seconds to sleep between readings
 */
uint16_t settings_get_sleep_time(void)
{
    return secondsBetweenReads;
}

/**
 * Set and store the time to sleep (in seconds)
 * 
 * If value given is 0, the value will be ignored and nothing will be set
 * 
 * STORES BUT DOES NOT COMMIT TO PERSISTANT STORAGE!
 *
 * @param timeInSeconds Value to store
 */
void settings_set_sleep_time(uint16_t timeInSeconds)
{
    if (timeInSeconds > 0) {
        secondsBetweenReads = timeInSeconds;
        store_sleep_time();
    }
}