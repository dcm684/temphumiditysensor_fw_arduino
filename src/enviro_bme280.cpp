#include "BME280I2C.h"

#include "enviro.h"

BME280I2C bmeSensor;

void printBME280Data
(
    Stream* client
)
{
    float temp(NAN), hum(NAN), pres(NAN);

    BME280::TempUnit tempUnit(BME280::TempUnit_Celsius);
    BME280::PresUnit presUnit(BME280::PresUnit_Pa);

    bmeSensor.read(pres, temp, hum, tempUnit, presUnit);

    client->print("Temp: ");
    client->print(temp);
    client->print("°"+ String(tempUnit == BME280::TempUnit_Celsius ? 'C' :'F'));
    client->print("\t\tHumidity: ");
    client->print(hum);
    client->print("% RH");
    client->print("\t\tPressure: ");
    client->print(pres);
    client->println("Pa");
}

/**
 * Initialized the BME280 sensor
 */
void BME280_initEnviroSensor(void)
{
    while(!bmeSensor.begin()) {
        Serial.println("Could not find BME280 sensor!");
        delay(1000);
    }

    switch(bmeSensor.chipModel())
    {
    case BME280::ChipModel_BME280:
        Serial.println("Found BME280 sensor! Success.");
        break;
    case BME280::ChipModel_BMP280:
        Serial.println("Found BMP280 sensor! No Humidity available.");
        break;
    default:
        Serial.println("Found UNKNOWN sensor! Error!");
    }
}

/**
 * Returns the temperature in Celsius
 */
float BME280_getTemperatureCelsius(void)
{
    return bmeSensor.temp(bmeSensor.TempUnit_Celsius);
}

/**
 * Returns the temperature compenstated humidity
 */
float BME280_getHumidity(void)
{
    return bmeSensor.hum();
}

/**
 * Returns the barometric pressure in hPa
 */
float BME280_getBarometricPressureHPA(void)
{
    return bmeSensor.pres(bmeSensor.PresUnit_hPa);
}

/**
 * Prints the temperature (in °C) and humidity (in %)
 */
void BME280_printEnviroResults(void)
{
    Serial.printf(ENVIRO_PRINT_STR_TEMPC_HUMID, 
                    BME280_getTemperatureCelsius(),
                    BME280_getHumidity());
}

/**
 * Simply calls printEnviroResults()
 *
 * BME280 does not need the extra delay like DHT needs so this function is
 * redundant
 */
void BME280_readEnviroSensor(void)
{
    BME280_printEnviroResults();
}
