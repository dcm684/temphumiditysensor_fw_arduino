#include "math_crc.h"
#include <stdint.h>

/**
 * CRC-8 - based on the CRC8 formulas by Dallas/Maxim
 * Code taken from http://www.leonardomiliani.com/en/2013/un-semplice-crc8-per-arduino/
 */
uint8_t CRC8(const uint8_t *data, uint8_t len)
{
    uint8_t crc = 0x00;
    while (len--) {
        uint8_t extract = *data++;
        for (uint8_t tempI = 8; tempI; tempI--) {
            uint8_t sum = (crc ^ extract) & 0x01;
            crc >>= 1;
            if (sum) {
                crc ^= 0x8C;
            }
            extract >>= 1;
        }
    }
    return crc;
}

/**
 * Calculate a 16-bit CRC
 * Taken from https://stackoverflow.com/a/23726131
 */
uint16_t CRC16(const uint8_t* data_p, uint8_t length)
{
    uint8_t x;
    uint16_t crc = 0xFFFF;

    while (length--){
        x = crc >> 8 ^ *data_p++;
        x ^= x>>4;
        crc = (crc << 8) ^ ((uint16_t)(x << 12)) ^ ((uint16_t)(x <<5)) ^ ((uint16_t)x);
    }
    return crc;
}