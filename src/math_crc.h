#ifndef MATH_CRC_H
#define MATH_CRC_H
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

uint8_t CRC8(const uint8_t *data, uint8_t len);
uint16_t CRC16(const uint8_t* data_p, uint8_t length);

#ifdef __cplusplus
}
#endif
#endif /* MATH_CRC_H */