#include <Arduino.h>
#include <EEPROM.h>
//#include <EEPROM_rotate.h>
#include <SPI.h>
#include <Wire.h>

#include "enviro.h"
#include "wifi_client.h"
#include "mqtt_client.h"
#include "device_status.h"
#include "mqtt_topic.h"
#include "settings.h"
#include "serial.h"

bool transmitReadings = true;

//EEPROM_Rotate persistentStorage;
EEPROMClass persistentStorage;

/**
 * Prepares the system ot enter deep sleep then enters deep sleep
 * 
 * Ends the EEPROM session which commits the values
 */
void enter_post_read_sleep(void)
{
    persistentStorage.end();

    Wire.endTransmission(true); /* Release the i2c circuit */

    uint64_t sleepTime;
    sleepTime = (uint64_t) settings_get_sleep_time();

    Serial.printf("Going into deep sleep for %u seconds", sleepTime);
    ESP.deepSleep(sleepTime * 1e6, WAKE_RF_DEFAULT); // 10e6 is 10 seconds
}

/**
 * Checks the reset reason and if it was a reset from deep sleep, true is
 * returned
 */
bool is_resuming_from_deep_sleep(void)
{
    uint32_t resetReason = ESP.getResetInfoPtr()->reason;

    // Serial.println(ESP.getResetReason());

    return resetReason == REASON_DEEP_SLEEP_AWAKE;
}

/**
 * Holds the program for some time to check for incoming UART commandsd
 */
void wait_for_and_handle_serial(void)
{
    unsigned long endTime = millis() + 5000;

    while (millis() < endTime) {
        check_and_process_serial();
    }
}

void setup()
{
    transmitReadings = true;

    persistentStorage.begin(512);

    init_serial();

    init_settings();

    /* If this is the first time run, stay awake for a short time to
        allow for serial communication */
    if (!is_resuming_from_deep_sleep()) {
        wait_for_and_handle_serial();
    }

    if (transmitReadings) {
        init_wifi();
    }

    Wire.begin();
    enviro_initEnviroSensor();
}

void loop()
{

    enviro_readEnviroSensor();

    if (transmitReadings) {

        /* Wait for a WiFi connection for 5 seconds before exiting */
        if (wifi_wait_for_connection(5000)) {

            if (mqtt_connect()) {

                mqtt_init_topics();
                mqtt_send_all_HA_config();
                mqtt_send_all_data();

                mqtt_loop();

                mqtt_disconnect();
            } else {
                Serial.println("Failed to connect to MQTT server");
            }
        }
    }

    enter_post_read_sleep();
}