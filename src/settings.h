#ifndef SETTINGS_H
#define SETTINGS_H

#include <ESP8266WiFi.h>

#ifdef __cplusplus
extern "C" {}
#endif

#define MAX_SSID_LEN 33 /**< Maximum length of SSID plus 1 null char */
#define MAX_WIFI_KEY_LEN 64 /* Max Wifi Key len plus 1 null char */
#define IP_BYTE_COUNT 4 /* Number of bytes in an IP address */
#define MAX_PORT_NUMBER 65535 /**< Maximum allowed IP port number */

#define MAX_DEVICE_NAME_LEN 30 /**< Maximum device name length */
#define MAX_MQTT_PREFIX_LEN 30 /**< Maximum MQTT topic prefix length */

/**<
 * @def SETTINGS_BYTES_USED
 * 
 * Number of EEPROM addresses used by the various settings
 * WiFi SSID (MAX_SSID_LEN)
 * WiFi Key (MAX_WIFI_KEY_LEN)
 * 4 IP addresses (4 bytes per)
 * Server port number (2 bytes)
 */
#define SETTINGS_BYTES_USED (MAX_SSID_LEN + MAX_WIFI_KEY_LEN + 4 * IP_BYTE_COUNT + 2)

void init_settings(void);

IPAddress* settings_get_mqtt_server_ip(void);
void settings_set_mqtt_server_ip(IPAddress *inAddr);

IPAddress* settings_get_mqtt_client_ip(void);
void settings_set_mqtt_client_ip(IPAddress *inAddr);

uint16_t settings_get_mqtt_server_port(void);
void settings_set_mqtt_server_port(uint16_t inPort);

char* settings_get_wifi_ssid(void);
void settings_set_wifi_ssid(char *inSSID);

char* settings_get_wifi_key(void);
void settings_set_wifi_key(char* inKey);

IPAddress* settings_get_wlan_gateway(void);
void settings_set_wlan_gateway(IPAddress *inAddr);

IPAddress* settings_get_wlan_subnet(void);
void settings_set_wlan_subnet(IPAddress *inAddr);

char* settings_get_device_name(void);
void settings_set_device_name(char *inDeviceName);
void settings_set_device_name_to_unique_name(void);

char* settings_get_mqtt_topic_prefix(void);
void settings_set_mqtt_topic_prefix(char *inDevicePrefix);

uint16_t settings_get_sleep_time(void);
void settings_set_sleep_time(uint16_t timeInSeconds);

#endif /* SETTINGS_H */