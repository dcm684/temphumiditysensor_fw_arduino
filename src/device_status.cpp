#include <Arduino.h>
#include <stdint.h>
#include <ESP8266WiFi.h>

#include "device_status.h"

/**
 * Read the device's supply voltage
 * 
 * @return Device's supply voltage
 */
int device_get_supply_voltage(void)
{
    return (analogRead(A0));
}