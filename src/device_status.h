#ifndef DEVICE_STATUS_H
#define DEVICE_STATUS_H
#include <stdint.h>

#ifdef __cplusplus
extern "C" {}
#endif

int device_get_supply_voltage(void);

#endif /* DEVICE_STATUS_H */