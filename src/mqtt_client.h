#ifndef MQTT_CLIENT
#define MQTT_CLIENT

bool mqtt_connect(void);
void mqtt_publish_str_to_topic(const char *toPub, const char *inTopic);
void mqtt_disconnect(void);
void mqtt_loop(void);
#endif /* MQTT_CLIENT */